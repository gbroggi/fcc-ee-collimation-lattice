"""
Plot SAD dynamic aperture
--------------------------------------------------------------------------------

This script takes the output of SAD's DynamicApertureSurvey and converts it to
python compatible datastructures.
The result can then be saved to twiss file where columns and rows corresponds to
sigmas of the two selected planes and the entries give the number of survived turns.
The script can then either display or save a plot od the DA. 

*--Required--*
- **da_file** *(str)*: Path to the SAD DA file.
- **planes** *(str)*: Planes for which DA is evaluated as string e.g XZ. Note that order is important.

*--Optional--*
- **emittance_ratio** *(float)*: Ratio between the vertical emittance epsilon_y and horizontal epsilon_x.
- **show_plot** *(bool)*: Flag whether Dynamic aperture plot should be displayed.
- **plot_file** *(str)*: Filename to which the DA plot will be saved.
- **tfs_file** *(str)*: Filename to which the DA tfs will be saved.

"""

from pathlib import Path
import tfs
import matplotlib.pyplot as plt 
import numpy as np
import numpy.ma as ma
import ast
import itertools
from generic_parser import EntryPointParameters, entrypoint

# Constants --------------------------------------------------------------------
PLANES=('X', 'Y', 'Z')
PLANECOMBINATIONS=[f"{x[0]}{x[1]}" for x in itertools.permutations(PLANES, 2)]
COLORMAP='magma'
PLOT_SUFFIX='.png'
SAD_SPACING_OFFPLANE=51

# Script arguments -------------------------------------------------------------
def get_params():
    params = EntryPointParameters()
    params.add_parameter(
        name="da_file",
        type=str,
        required=True,
        help="Path to the SAD DA file.",
    )
    params.add_parameter(
        name="planes",
        type=str,
        required=True,
        choices=PLANECOMBINATIONS,
        help="Planes for which DA is evaluated as string e.g XZ. Note that order is important.",
    )
    params.add_parameter(
        name="emittance_ratio",
        type=float,
        default=1.,        
        help="Ratio between the vertical emittance epsilon_y and horizontal epsilon_x.",
    )
    params.add_parameter(
        name="show_plot",
        action="store_true",
        help="Flag whether Dynamic aperture plot should be displayed.",
    )
    params.add_parameter(
        name="plot_file",
        type=str,
        default=None,
        help="Filename to which the DA plot will be saved.",
    )
    params.add_parameter(
        name="tfs_file",
        type=str,
        default=None,
        help="Filename to which the DA tfs will be saved.",
    )
    
    return params

# Entrypoint -------------------------------------------------------------------
@entrypoint(get_params(), strict=True)
def main(opt):
    opt = _check_opts(opt)
    da_df = process_da_file(opt.da_file, opt.planes, opt.emittance_ratio, opt.tfs_file)
    return plot_da(da_df, opt.planes, opt.show_plot, opt.plot_file, opt.emittance_ratio)


def _check_opts(opt):
    
    if not opt.show_plot and opt.plot_file==None:
        raise ValueError("DA Plot is neither displayed nor saved anywhere.")

    if Path(opt.da_file).is_file():    
            opt["da_file"]=Path(opt.da_file)
    else:
        raise OSError("DA file path appears to be invalid")
    
    opt=_convert_str_to_path(opt, "plot_file")
    opt=_convert_str_to_path(opt, "tfs_file")

    return opt


def _convert_str_to_path(opt, file):
    if opt[file]!=None:
            opt[file]=Path(opt[file])
    return opt


def process_da_file(da_path, planes, emittance_ratio, tfs_path=None):
    
    with open(da_path) as dafile:
        dadata = dafile.read()

    da_data=np.array(ast.literal_eval(dadata.replace("{","[").replace("}","]")), dtype='object')
    track_data =np.array(da_data[1][1], dtype='object')
    turn_data =np.array([track_data[i][2] for i in range(len(track_data))], dtype='int')

    max_mask=ma.masked_where(turn_data==2**29, turn_data)
    turn_data[max_mask.mask]=max_mask.max()

    data_df = tfs.TfsDataFrame(data=turn_data,
                               columns=[ f"SIGMA{planes[1]}_{i}" for i in range(turn_data.shape[1])],
                               index=track_data[:,0],
                               headers={"Score":da_data[0],
                                        "Planes":planes,
                                        "Emittance_ratio":emittance_ratio,
                                        "XRange":str(da_data[1][0][0]),
                                        "YRange":str(da_data[1][0][1]),
                                        "ZRange":str(da_data[1][0][2])}
                                )
    data_df['DA']=track_data[:,1]   
    if tfs_path!=None:
        tfs.write(tfs_path, data_df, save_index=f"SIGMA{planes[0]}")                         
    return data_df
    

def plot_da(da_df, plane, show_plot, plot_file, emittance_ratio):

    range_dict={ pl:ast.literal_eval(da_df.headers[f"{pl}Range"]) for pl in PLANES }
    range_dict['Y']=[range_dict['Y'][0]/np.sqrt(emittance_ratio), range_dict['Y'][1]/np.sqrt(emittance_ratio)]

    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(9,9), constrained_layout=True)

    turn_columns=[col for col in da_df.columns if f'SIGMA{plane[-1]}' in col]

    cs = ax.contourf(da_df.index.to_numpy(),
                     np.linspace(range_dict[plane[-1]][0], range_dict[plane[-1]][-1], SAD_SPACING_OFFPLANE),
                     da_df[turn_columns].to_numpy().T,
                     cmap=COLORMAP)

    cbar=plt.colorbar(cs)
    cbar.ax.set_ylabel("No. of surviving turns", fontsize=16)
    cbar.ax.tick_params(axis='both', which='major', labelsize=12)
    ax.plot(da_df.index,
            da_df["DA"]*(range_dict[plane[-1]][-1])/SAD_SPACING_OFFPLANE,
            linewidth=2,
            color='red',
            marker='X')
    ax.set_xlabel(f"$\sigma_{plane[0]}$", fontsize=16)
    ax.set_ylabel(f"$\sigma_{plane[1]}$", fontsize=16)
    ax.tick_params(axis='both', which='major', labelsize=12)

    if plot_file != None:
        plt.savefig(plot_file.with_suffix(PLOT_SUFFIX))
    if show_plot:
        plt.show()

    return fig, ax



# Script Mode ------------------------------------------------------------------

if __name__ == "__main__":
    main()
