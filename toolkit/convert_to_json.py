import xtrack as xt
import numpy as np
from cpymad.madx import Madx
from pathlib import Path
from generic_parser import EntryPointParameters, entrypoint

def get_params():
    params = EntryPointParameters()
    params.add_parameter(
        name="sequence_file",
        type=str,
        required=True,
        help="Path to the madx sequence file.",
    )
    params.add_parameter(
        name="operation_mode",
        type=str,
        required=True,
        help="Which FCC-ee operation mode.",
    )

    return params

@entrypoint(get_params(), strict=True)
def main(opt):
    if opt.operation_mode == 'z':
        p0c = 45.6e9 # eV
    elif opt.operation_mode == 't':
        p0c = 182.5e9 # eV
    else:
        raise ValueError(f"Operation mode {opt.operation_mode} is not supported. Supported operation modes are 'z' and 't'")

    filename = Path(opt.sequence_file)

    mad = Madx()
    mad.call(opt.sequence_file)
    mad.beam()
    mad.use('L000017')

    # Convert the MAD-X sequence to an Xtrack line
    line = xt.Line.from_madx_sequence(mad.sequence.L000017, install_apertures=True, deferred_expressions=True)
    line.particle_ref = xt.Particles(q0=1, mass0=xt.ELECTRON_MASS_EV, p0c=p0c)

    if 'thin' in opt.sequence_file:
        # Dipole edges miss aperture markers before and after
        insert_dipoleEdge_bounding_apertures(line)
    
    fix_collimator_apertures(line)

    # Save to json
    line.to_json(f'{filename.name}.json')


# Auxiliary functions ----------------------------------------------------------

def find_apertures(line):
    i_apertures = []
    apertures = []
    for ii, ee in enumerate(line.elements):
        if ee.__class__.__name__.startswith('Limit'):
            i_apertures.append(ii)
            apertures.append(ee)
    return np.array(i_apertures), np.array(apertures)


def find_dipoleEdges(line):
    i_apertures = []
    apertures = []
    for ii, ee in enumerate(line.elements):
        if ee.__class__.__name__.startswith('DipoleEdge'):
            i_apertures.append(ii)
            apertures.append(ee)
    return np.array(i_apertures), np.array(apertures)


def insert_dipoleEdge_bounding_apertures(line):
    # Place aperture defintions around all dipole edges in order to ensure
    # the correct functioning of the aperture loss interpolation
    # the aperture definitions are taken from the nearest neighbour aperture in the line
    s_pos = line.get_s_elements(mode='upstream')
    apert_idx, apertures = find_apertures(line)
    apert_s = np.take(s_pos, apert_idx)

    dipoleEdge_idx, dipoleEdges = find_dipoleEdges(line)
    dipoleEdge_names = np.take(line.element_names, dipoleEdge_idx)
    dipoleEdge_s_start = np.take(s_pos, dipoleEdge_idx)
    dipoleEdge_s_end = np.take(s_pos, dipoleEdge_idx + 1)

    # Find the nearest neighbour aperture in the line
    dipoleEdge_apert_idx_start = np.searchsorted(apert_s, dipoleEdge_s_start, side='left')
    dipoleEdge_apert_idx_end = dipoleEdge_apert_idx_start + 1

    aper_start = apertures[dipoleEdge_apert_idx_start]
    aper_end = apertures[dipoleEdge_apert_idx_end]

    idx_offset = 0
    for ii in range(len(dipoleEdges)):
        line.insert_element(name=dipoleEdge_names[ii] + '_aper_start',
                            element=aper_start[ii].copy(),
                            at=dipoleEdge_idx[ii] + idx_offset)
        idx_offset += 1

        line.insert_element(name=dipoleEdge_names[ii] + '_aper_end',
                            element=aper_end[ii].copy(),
                            at=dipoleEdge_idx[ii] + 1 + idx_offset)
        idx_offset += 1


def fix_collimator_apertures(line):
    # Fix collimator apertures: 
    # collimator apertures need to be the beam pipe aperture at the collimator location
    # the actual collimator half gaps are defined by the collimator settings in a collimator database (CollDB)
    newLine = xt.Line(elements=[], element_names=[])

    coll_prefixes = ('tcp.', 'tcs.', 'tct.', 'tcr.') 
    # NOTE: SR masks (tcrm.) are currently treated as aperture restrictions only
    for ee, nn in zip(line.elements, line.element_names):
        if nn.startswith(coll_prefixes) and nn.endswith('_aper'):
            if 'h' in nn:
                bp_aper = ee.max_y
            elif 'v' in nn:
                bp_aper = ee.max_x

            newLine.append_element(xt.LimitEllipse(a=bp_aper, b=bp_aper), nn)
        else:
            newLine.append_element(ee, nn)

    # Update the line in place
    line.element_names = newLine.element_names
    line.element_dict.update(newLine.element_dict)


# Script Mode ------------------------------------------------------------------

if __name__ == "__main__":
    main()