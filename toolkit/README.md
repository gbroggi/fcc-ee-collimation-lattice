# FCC-ee collimation lattice toolkit

Collection of scripts that help in common first analysis steps.
Scripts are written to work in conjuction with the example sad/MAD-X scripts in the `tests` folder.

## Prerequisites

Scripts with the file extension `.n` are SAD modules, which have to be imported into the main SAD script via

```
Get["../toolkit/MODULE.n"];
```

and then the function can be called via

```
Function[Parameter];
```

Scripts ending on `.py` are stand-alone `python3.7` scripts, used to post-process data.
To run those, several packages need to be installed, which is done via

```bash
pip install matplotlib numpy scipy tfs-pandas generic-parser 
```

## Scripts

- *SAD Scripts*
    - `SAD2MADX.n` - converts the currently active SAD sequence into MAD-X format and saves it into specified `fname`
    - `tunematching.n` - matches the tunes `n_x` & `n_y` of the FCC-ee collider ring to the specified values using the quadrupoles in the RF-straights
    - `twiss.n` - saves the last optics functions from SAD's `CALCULATE;` to a `twiss`-file `filename`
    - `closed_orbit.n` - prints the closed orbit from SAD's `Emittance[];` to a `twiss` like file with the name `filename`
- *Python Scripts*
    - `place_collimators.py` - using the `twiss`-file of the FCC-ee and given the collimator opening and position of the primary collimator calculates the optimal phase advance and positions of the secondaries in the lattice and returns MAD-X installation script
    - `plot_da.py` - provided the DA from the `check_da.sad` script, plots the dynamic aperture and optionally saves the data in `.tfs` format
    - `plot_lattice.py` - given a `twiss`-file from one of the `optics`-scripts in the `tests` folder, plots the optics functions and saves them as `.png`
    