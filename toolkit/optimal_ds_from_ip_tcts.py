import xtrack as xt
import tfs
import numpy as np
from scipy.optimize import minimize
from functools import partial


# Constants --------------------------------------------------------------------

NUMBER_OF_IPS = 4
TOLERANCE_MU = 1E-3 # rad / (2*pi)
L_TANK = 1.0 # m Assumed length of the collimator tank
L_RCOILS = 0.1 # m Length of the returning coils
S_MARGIN = (L_TANK / 2) + L_RCOILS
OPTIMAL_DMU = np.arange(0.5, 2.5, 0.5) # pi, 2pi, 3pi and 4pi


def _driftlength_from_phaseadvance(beta0, alpha0, mu):
    return np.tan(mu * 2 * np.pi) * beta0 / (1 + np.tan(mu * 2 * np.pi) * alpha0)


def _phaseadvance_from_driftlength(beta0, alpha0, dl):
    return 1 / (2 * np.pi) * np.arctan(dl / (beta0 - alpha0 * dl))


def get_aperture_bottleneck(line):
    aperture_bottleneck = {}

    aper_df = tfs.read('../scripts/aperture_z_b1_nottapered.tfs')
    aperture_bottleneck['H'] = {'element_name': aper_df.AT_ELEMENT.split('.')[0].lower() + '.1',
                                'min_bsc': aper_df.N1MIN}

    aper_df = tfs.read('../scripts/aperture_z_b1_nottapered_vertical.tfs')
    aperture_bottleneck['V'] = {'element_name': aper_df.AT_ELEMENT.split('.')[0].lower() + '.1',
                                'min_bsc': aper_df.N1MIN}

    return aperture_bottleneck


def get_tcr_names(line):
    tcr_names = []
    for nn in line.element_names:
        if nn.startswith('tcr.') and nn.endswith('.b1'):
            tcr_names.append(nn)

    tcr_names = tcr_names[:int(len(tcr_names)/NUMBER_OF_IPS)]
    
    return tcr_names


def get_phases_elements_to_protect(tw, tcr_names, aperture_bottleneck):
    mux_elements_to_protect = {}
    muy_elements_to_protect = {}
    for tcr_name in tcr_names:
        if 'h' in tcr_name:
            mux_elements_to_protect[tcr_name] = tw['mux', tcr_name]
        elif 'v' in tcr_name:
            muy_elements_to_protect[tcr_name] = tw['muy', tcr_name]

    mux_elements_to_protect[aperture_bottleneck['H']['element_name']] = tw['mux', aperture_bottleneck['H']['element_name']]
    muy_elements_to_protect[aperture_bottleneck['V']['element_name']] = tw['muy', aperture_bottleneck['V']['element_name']]

    return mux_elements_to_protect, muy_elements_to_protect


def get_long_drifts(line):
    tab = line.get_table()

    s_first_tcr = tab.rows['tcr.*']['s'][0]

    long_drifts = []
    for ee, nn in zip(line.elements, line.element_names):
        if isinstance(ee, xt.Drift) and tab['s', nn] < s_first_tcr:
            if ee.length >= 1:
                long_drifts.append(nn)

    return long_drifts


def get_phase_ranges(tab, tw, long_drifts):
    mux_range_tcth, muy_range_tctv = [], []

    for long_drift in long_drifts:
        element_downstream = tab.rows[long_drift:]['name'][1]

        mux_margin = _phaseadvance_from_driftlength(tw['betx', long_drift], tw['alfx', long_drift], S_MARGIN)
        mux_range = (tw['mux', long_drift] + mux_margin, tw['mux', element_downstream] - mux_margin)
        mux_range_tcth.append(mux_range)

        muy_margin = _phaseadvance_from_driftlength(tw['bety', long_drift], tw['alfy', long_drift], S_MARGIN)
        muy_range = (tw['muy', long_drift] + muy_margin, tw['muy', element_downstream] - muy_margin)
        muy_range_tctv.append(muy_range)

    return mux_range_tcth, muy_range_tctv


def get_optimal_tct_phases(optimal_dmu, mux_elements_to_protect, muy_elements_to_protect):

    def _objective_function(mu, optimal_dmu, mu_elements_to_protect):
        return sum((mu_value - mu - optimal_dmu)**2 for mu_value in mu_elements_to_protect.values())
    
    optimal_mux_tcth, optimal_muy_tctv = [], []

    for dmu in optimal_dmu:
        dmu_guess = 0.0

        # Partially apply the objective function to fix dmu and mux_elements_to_protect
        mux_objective = partial(_objective_function, optimal_dmu=dmu, mu_elements_to_protect=mux_elements_to_protect)
        muy_objective = partial(_objective_function, optimal_dmu=dmu, mu_elements_to_protect=muy_elements_to_protect)

        # Optimize for mux
        result = minimize(mux_objective, dmu_guess)
        optimal_mux_tcth.append(result.x[0])

        # Optimize for muy
        result = minimize(muy_objective, dmu_guess)
        optimal_muy_tctv.append(result.x[0])

    return optimal_mux_tcth, optimal_muy_tctv


def find_optimal_drifts(drifts_list, optimal_mux_tcth, mux_range_tcth, optimal_muy_tctv, muy_range_tctv):
    optimal_tcth_drift_found = False
    optimal_tctv_drift_found = False

    for optimal_mux, optimal_muy in zip(optimal_mux_tcth, optimal_muy_tctv):
        if not optimal_tcth_drift_found:
            optimal_drift_dict_tcth = {'optimal_mu': optimal_mux}
            for ii, rr in enumerate(reversed(mux_range_tcth)):
                idx = len(mux_range_tcth) - 1 - ii
                if rr[0] - TOLERANCE_MU <= optimal_mux <= rr[1] + TOLERANCE_MU:
                    optimal_tcth_drift_found = True
                    optimal_drift_dict_tcth['optimal_drift'] = drifts_list[idx]
                    optimal_drift_dict_tcth['mu_range'] = rr
                    break

        if not optimal_tctv_drift_found:
            optimal_drift_dict_tctv = {'optimal_mu': optimal_muy}
            for ii, rr in enumerate(reversed(muy_range_tctv)):
                idx = len(muy_range_tctv) - 1 - ii
                if rr[0] - TOLERANCE_MU <= optimal_muy <= rr[1] + TOLERANCE_MU:
                    optimal_tctv_drift_found = True
                    optimal_drift_dict_tctv['optimal_drift'] = drifts_list[idx]
                    optimal_drift_dict_tctv['mu_range'] = rr
                    break

    if optimal_tcth_drift_found and optimal_tctv_drift_found:
        return optimal_drift_dict_tcth, optimal_drift_dict_tctv
    else:
        raise ValueError('Could not find suitable locations for TCTH and TCTV.')
        

def find_optimal_tct_positions(tw, optimal_drift_tcth, optimal_drift_tctv):
    if optimal_drift_tcth['mu_range'][0] <= optimal_drift_tcth['optimal_mu'] <= optimal_drift_tcth['mu_range'][1]:
        dmux = optimal_drift_tcth['optimal_mu'] - tw['mux', optimal_drift_tcth['optimal_drift']]
        ds = _driftlength_from_phaseadvance(tw['betx', optimal_drift_tcth['optimal_drift']],
                                            tw['alfx', optimal_drift_tcth['optimal_drift']],
                                            dmux)
        s_tcth = tw['s', optimal_drift_tcth['optimal_drift']] + ds
    else:
        # The optimal phase is outside the drift
        # Place the collimator at the beginning/end of the drift
        if abs(optimal_drift_tcth['optimal_mu'] - optimal_drift_tcth['mu_range'][0]) < abs(optimal_drift_tcth['optimal_mu'] - optimal_drift_tcth['mu_range'][1]):
            # Optimal phase is closer to the beginning of the drift
            s_tcth = tw['s', optimal_drift_tcth['optimal_drift']] + S_MARGIN
        else:
            # Optimal phase is closer to the end of the drift
            s_tcth = tw['s', optimal_drift_tcth['optimal_drift']] - S_MARGIN


    if optimal_drift_tctv['mu_range'][0] <= optimal_drift_tctv['optimal_mu'] <= optimal_drift_tctv['mu_range'][1]:
        dmuy = optimal_drift_tctv['optimal_mu'] - tw['muy', optimal_drift_tctv['optimal_drift']]
        ds = _driftlength_from_phaseadvance(tw['bety', optimal_drift_tctv['optimal_drift']],
                                            tw['alfy', optimal_drift_tctv['optimal_drift']],
                                            dmuy)
        s_tctv = tw['s', optimal_drift_tctv['optimal_drift']] + ds
    else:
        # The optimal phase is outside the drift
        # Place the collimator at the beginning/end of the drift
        if abs(optimal_drift_tctv['optimal_mu'] - optimal_drift_tctv['mu_range'][0]) < abs(optimal_drift_tctv['optimal_mu'] - optimal_drift_tctv['mu_range'][1]):
            # Optimal phase is closer to the beginning of the drift
            s_tctv = tw['s', optimal_drift_tctv['optimal_drift']] + S_MARGIN
        else:
            # Optimal phase is closer to the end of the drift
            s_tctv = tw['s', optimal_drift_tctv['optimal_drift']] - S_MARGIN

    return s_tcth, s_tctv


def compute_optimal_ds_from_ip(line_thick):
    tab = line_thick.get_table()
    tw = line_thick.twiss4d()

    tcr_names = get_tcr_names(line_thick)
    aperture_bottleneck = get_aperture_bottleneck(line_thick)

    mux_elements_to_protect, muy_elements_to_protect = get_phases_elements_to_protect(tw, tcr_names, aperture_bottleneck)

    long_drifts = get_long_drifts(line_thick)

    mux_range_tcth, muy_range_tctv = get_phase_ranges(tab, tw, long_drifts)

    optimal_mux_tcth, optimal_muy_tctv = get_optimal_tct_phases(OPTIMAL_DMU, mux_elements_to_protect, muy_elements_to_protect)

    optimal_drift_dict_tcth, optimal_drift_dict_tctv = find_optimal_drifts(long_drifts, optimal_mux_tcth, mux_range_tcth, optimal_muy_tctv, muy_range_tctv)

    s_tcth, s_tctv = find_optimal_tct_positions(tw, optimal_drift_dict_tcth, optimal_drift_dict_tctv)

    s_first_ip = tab.rows['ip.*']['s'][tab.rows['ip.*']['s'] > 10000][0]

    optimal_ds_from_ip_tcth = s_first_ip - s_tcth
    optimal_ds_from_ip_tctv = s_first_ip - s_tctv

    return optimal_ds_from_ip_tcth, optimal_ds_from_ip_tctv
















