import xtrack as xt
import numpy as np
import optimal_ds_from_ip_tcts
from pathlib import Path
from generic_parser import EntryPointParameters, entrypoint

# Constants --------------------------------------------------------------------

R_BEAMPIPE = 0.03 # m

# TODO: make this more general ?
S_IP = {'IPA': 67993.88178257,
        'IPD': 90658.50904341,
        'IPG': 22664.62726086,
        'IPJ': 45329.25452171}

TCT_NAMES = {'IPA': {'H': 'tct.h.1.b1', 'V': 'tct.v.1.b1'},
             'IPD': {'H': 'tct.h.2.b1', 'V': 'tct.v.2.b1'},
             'IPG': {'H': 'tct.h.3.b1', 'V': 'tct.v.3.b1'},
             'IPJ': {'H': 'tct.h.4.b1', 'V': 'tct.v.4.b1'}}


# Script arguments -------------------------------------------------------------

def get_params():
    params = EntryPointParameters()
    params.add_parameter(
        name="thick_line",
        type=str,
        required=True,
        help="Path to the xtrack thick line."
    )
    params.add_parameter(
        name="thin_line",
        type=str,
        required=True,
        help="Path to the xtrack thin line."
    )

    return params


# Entrypoint -------------------------------------------------------------------

@entrypoint(get_params(), strict=True)
def main(opt):
    line_thick = xt.Line.from_json(opt.thick_line)
    line = xt.Line.from_json(opt.thin_line)

    optimal_ds_from_ip_tcth, optimal_ds_from_ip_tctv = optimal_ds_from_ip_tcts.compute_optimal_ds_from_ip(line_thick)

    insert_collimators(line, optimal_ds_from_ip_tcth, optimal_ds_from_ip_tctv)

    insert_collimator_apertures(line, optimal_ds_from_ip_tcth, optimal_ds_from_ip_tctv)

    filename = Path(opt.thin_line)
    # Overwrite the original line with the new one with TCTs
    line.to_json(f'{filename.name}')


def insert_collimators(line, optimal_ds_from_ip_tcth, optimal_ds_from_ip_tctv):
    tct = xt.Drift()

    for IP_name, tct_name in TCT_NAMES.items():
        line.insert_element(tct_name['H'], tct, at_s=S_IP[IP_name] - optimal_ds_from_ip_tcth)
        line.insert_element(tct_name['V'], tct, at_s=S_IP[IP_name] - optimal_ds_from_ip_tctv)


def insert_collimator_apertures(line, optimal_ds_from_ip_tcth, optimal_ds_from_ip_tctv):
    for IP_name, tct_name in TCT_NAMES.items():
        tct_aper = xt.LimitEllipse(a=R_BEAMPIPE, b=R_BEAMPIPE)

        line.insert_element(tct_name['H'] + '_aper', tct_aper.copy(), at_s=S_IP[IP_name] - optimal_ds_from_ip_tcth)
        line.insert_element(tct_name['V'] + '_aper', tct_aper.copy(), at_s=S_IP[IP_name] - optimal_ds_from_ip_tctv)


# Script Mode ------------------------------------------------------------------

if __name__ == "__main__":
    main()