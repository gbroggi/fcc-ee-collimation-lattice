import xtrack as xt
import numpy as np
from generic_parser import EntryPointParameters, entrypoint


def get_params():
    params = EntryPointParameters()
    params.add_parameter(
        name="line",
        type=str,
        required=True,
        help="Path to the xtrack line.",
    )
    params.add_parameter(
        name="operation_mode",
        type=str,
        required=True,
        help="Which FCC-ee operation mode.",
    )
    params.add_parameter(
        name="target_gemitt_y",
        type=float,
        required=True,
        help="Target vertical (geometric) emittance in m.",
    )

    return params

@entrypoint(get_params(), strict=True)
def main(opt):
    line = xt.Line.from_json(opt.line)

    line.build_tracker()

    delta0 = get_sr_compensation_delta(line)

    line = match_vertical_emittance(line, delta0, opt.target_gemitt_y)

    line.to_json(f'tapered_{opt.operation_mode}_b1_thin_{opt.target_gemitt_y*1e12}pm.seq.json')


# Auxiliary functions ----------------------------------------------------------


def get_sr_compensation_delta(line):
    line.configure_radiation(model='mean')
    line.compensate_radiation_energy_loss() 
    tw = line.twiss(eneloss_and_damping=True)
    delta0 = tw.delta[0] - np.mean(tw.delta)
    print(f"sr_compensation_delta: {delta0}")

    return delta0


def match_vertical_emittance(line, delta0, target_gemitt_y):
    line.configure_radiation(model='mean')
    line.compensate_radiation_energy_loss(delta0=delta0)
    
    opt = line.match(
        solve=True,
        eneloss_and_damping=True,
        compensate_radiation_energy_loss=True,
        delta0=delta0,
        targets=[
            xt.Target(eq_gemitt_y=target_gemitt_y, tol=1e-18, optimize_log=True)],
        vary=xt.Vary('on_wiggler_v', step=0.001, limits=(0.01, 0.8))
    )
    print(f"Emittance successfully matched with 'on_wiggler_v'={line.vars.val['on_wiggler_v']}")
    opt.target_status()

    return line


# Script Mode ------------------------------------------------------------------

if __name__ == "__main__":
    main()