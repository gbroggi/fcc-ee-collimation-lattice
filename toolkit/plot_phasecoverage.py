"""
Plot coverage in phasespace
--------------------------------------------------------------------------------

Plot the coverage in normalized phase space for a given number of elements.

*--Required--*
- **twiss_file** *(str)*: Path to the twiss file with optics functions and aperture for required elements defined.
- **plane** *(str)*: Select to plane for which the phase space will be plotted (either 'x' or 'y').
- **reference_element** *(str)*: Element which is used as reference for the phase calculation.
- **secondary_elements** *(str)*: (Regex-)String to select other elements to include.

*--Optional--*

- **limit** *(float)*: Limits on the radial axis.
- **show_plot** *(bool)*: Flag whether to display the plot.
- **plot_file** *(str)*: Filename to which the plot will be saved.

"""
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt 
import pandas as pd
import tfs
from generic_parser import EntryPointParameters, entrypoint

# Constants --------------------------------------------------------------------

PLOT_SUFFIX='.png'
COLORMAP='viridis'
PLANES = ['X', 'Y']
PLANE_TO_APER = {'X': 'APER_1', 'Y': 'APER_2'}


# Script arguments -------------------------------------------------------------
def get_params():
    params = EntryPointParameters()
    params.add_parameter(
        name="twiss_file",
        type=str,
        required=True,
        help="Path to the twiss file.",
    )
    params.add_parameter(
        name="plane",
        type=str,
        choices=PLANES,
        required=True,
        help="Coverage in which plane.",
    )
    params.add_parameter(
        name="reference_element",
        type=str,
        required=True,
        help="Name of primary element. Will be placed at 0 degree and used as phase reference for other elements.",
    )
    params.add_parameter(
        name="secondary_elements",
        type=str,
        required=True,
        help="Regex string for secondary elements.",
    )
    params.add_parameter(
        name="limit",
        type=float,
        default=20.,
        help="Max. radius.",
    )
    params.add_parameter(
        name="show_plot",
        action="store_true",
        help="Flag whether to display the plot.",
    )
    params.add_parameter(
        name="plot_file",
        type=str,
        default=None,
        help="Filename to which the plot will be saved.",
    )
    params.add_parameter(
        name="legend",
        action="store_true",
        help="Add legend to plot.",
    )
    return params


# Entrypoint -------------------------------------------------------------------
@entrypoint(get_params(), strict=True)
def main(opt):
    opt = _check_opts(opt)
    twiss_df = tfs.read(opt.twiss_file, index='NAME')

    reference_element_df = set_reference_element(twiss_df, opt.reference_element, opt.plane)

    twiss_df = add_radius_and_phase(twiss_df, reference_element_df, opt.secondary_elements, opt.plane)

    return plot_coverage(twiss_df,
                        opt.limit,
                        opt.plot_file,
                        opt.legend,
                        opt.show_plot)


def _check_opts(opt):

    if not opt.show_plot and opt.plot_file==None:
        raise ValueError("Plot is neither displayed nor saved anywhere.")

    if Path(opt.twiss_file).is_file():    
        opt["twiss_file"]=Path(opt.twiss_file)
    else:
        raise OSError("Twiss file path appears to be invalid")

    opt=_convert_str_to_path(opt, "plot_file")

    return opt


def _convert_str_to_path(opt, file):
    if opt[file]!=None:
        opt[file]=Path(opt[file])
    return opt


def set_reference_element(twiss_df, reference_element, plane):

    reference_element_df = twiss_df.loc[reference_element]

    aperture=reference_element_df[PLANE_TO_APER[plane]]
    sigma = (np.sqrt(twiss_df[f'E{plane}']*reference_element_df[f'BET{plane}']))

    reference_element_df["PHASE"] = 0.0
    reference_element_df["RADIUS"] = aperture/sigma

    return reference_element_df


def add_radius_and_phase(twiss_df, reference_element_df, secondary_elements, plane):
    twiss_df = twiss_df[twiss_df.index.str.contains(secondary_elements)]
    twiss_df.loc[twiss_df['APER_2']==0, 'APER_2'] = twiss_df.loc[twiss_df['APER_2']==0, 'APER_1']

    twiss_df.loc[:, 'RADIUS'] = twiss_df[PLANE_TO_APER[plane]]/np.sqrt(twiss_df[f'E{plane}']*twiss_df[f'BET{plane}'])
    twiss_df.loc[:, 'PHASE']  = np.mod(twiss_df[f'MU{plane}'] - reference_element_df[f'MU{plane}'], 1)*2*np.pi

    twiss_df = pd.concat([reference_element_df.to_frame().T, twiss_df])
    return twiss_df


def plot_coverage(twiss_df, limit, plot_file, legend, show_plot):

    fig, ax = plt.subplots(nrows=1,
                           ncols=1,
                           figsize=(9, 9),
                           constrained_layout=True,
                           subplot_kw={'projection': 'polar'}
                           )
    cmap = plt.cm.get_cmap(COLORMAP, len(twiss_df))
    twiss_df['COLOR'] = [cmap(x) for x in range(len(twiss_df))]

    for idx, data in twiss_df.iterrows():
        ax.scatter(data['PHASE'], data['RADIUS'], 250, label=idx, marker='X', color=data['COLOR'])
        ax.scatter(data['PHASE']-np.pi, data['RADIUS'], 250, marker='X', color=data['COLOR'])

        angle = np.arccos(data['RADIUS']/limit)
        angles = np.array([angle,0,-angle])

        ax.plot(angles + data['PHASE'], [limit,data['RADIUS'], limit], color=data['COLOR'], linestyle=':', linewidth=5)
        ax.plot(angles + data['PHASE'] - np.pi, [limit,data['RADIUS'], limit], color=data['COLOR'], linestyle=':', linewidth=5)

    ax.set_rmax(limit)
    ax.grid(True)
    if legend:
        ax.legend(loc="lower left", fontsize=25)
    ax.tick_params(axis='both', which='major', labelsize=15)

    if plot_file != None:
        plt.savefig(plot_file.with_suffix(PLOT_SUFFIX))
    if show_plot:
        plt.show()

    return fig, ax


# Script Mode ------------------------------------------------------------------

if __name__ == "__main__":
    main()
