# FCC-ee collimation lattice

Repository for experimental FCC-ee lattices with collimation section

The original, unmodified FCC-ee lattices are stored in the `master` branch, whereas
development of new layouts and optics takes place in branches.

The plain sequences are in the `sequences` directory, split up in accelerator codes.

In the `scripts` directory, example scripts can be found for how to load the sequences,
match tunes, DA studies, and translation from the original SAD lattice to a MAD-X sequence file.
The output of these scripts can then be tested against reference values, using `python-pytest` test script from the `tests` directory.

The `toolkit` directory contains additional scripts required in some of the example scripts or
to create plots.

After each commit, a gitlab pipeline is triggered, with the different steps defined in `.gitlab-ci.yml`.
The pipeline starts by downloading the latest MAD-X and SAD version in the `build`-stage.
In the `test`-stage, the MAD-X and SAD scripts in the `tests` directory are run, with different jobs for the working points.
The output of the scripts is then tested against known references defined in `tests/reference_file.json`.
Files specified in `artifacts` are saved to be able to download via the gitlab website, and are handed to the jobs of the `deploy` stage, where plots are produced, which are again saved as artifacts.
