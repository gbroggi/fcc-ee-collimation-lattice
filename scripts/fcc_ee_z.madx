!--------------------------------------------------------------
! Example FCC-ee MAD-X script
! This script prepares the Z (E=45.6 GeV) lattice for ensuring tracking studies.
! The thick sequences is loaded.
! Depending on the settings, wigglers for vertical emittance generation are installed.
! Wiggler parameters need to be adjusted a posteriori according to the target emittance.
! A thin version for tracking will be exported.
!--------------------------------------------------------------
SET, FORMAT="19.15f";
option,update_from_parent=true; // new option in mad-x as of 2/2019

!--------------------------------------------------------------
! General settings
!--------------------------------------------------------------
install_wigglers = 0; // 0 for False; 1 for True

!--------------------------------------------------------------
! Lattice selection and beam parameters
!--------------------------------------------------------------
CALL, FILE="../sequences/MAD-X/FCCee_z_624_nosol_9.seq";

pbeam :=   45.6;
EXbeam = 0.71e-9;
EYbeam = 2.1e-12;
Nbun :=    11200;
NPar :=   2.18e11;

Ebeam := sqrt( pbeam^2 + emass^2 );

// Beam defined without radiation
BEAM, PARTICLE=POSITRON, NPART=Npar, KBUNCH=Nbun, ENERGY=Ebeam, RADIATE=FALSE, BV=+1, EX=EXbeam, EY=EYbeam;

!--------------------------------------------------------------
! Load aperture definitions and perform sequence edits
!--------------------------------------------------------------
// Load the aperture definition
CALL, FILE="../sequences/MAD-X/aperture/FCCee_aper_definitions.madx";

! Cycle the line away from IP.1 to avoid negative drift between IP.1 and START_EXP_CHAMBER_1
SEQEDIT, SEQUENCE=L000017;
    CYCLE, START=PQC2RE.1;
ENDEDIT;

CALL, FILE="../sequences/MAD-X/aperture/install_exp_chamber.madx";

! Cycle back to IP.1
SEQEDIT, SEQUENCE=L000017;
    CYCLE, START=IP.1;
ENDEDIT;

!-------------------------------------------------------------------------------
! Perform initial TWISS and survey in an ideal machine without radiation
!-------------------------------------------------------------------------------
USE, SEQUENCE = L000017;

// Save the voltage settings for the cavities for later use if needed
VOLTCA1SAVE = VOLTCA1; 
SHOW, VOLTCA1SAVE;

// Turn off the cavities for ideal machine twiss and survey
VOLTCA1 = 0;

TWISS, TOLERANCE=1E-12; ! Twiss without radiation and tapering
SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L,K0L,K1L,K2L,K3L,K4L,K1SL,K2SL,K3SL,K4SL,HKICK,VKICK,BETX,BETY,ALFX,ALFY,MUX,MUY,DX,DY,DPX,DPY,R11,R12,R22,R21,X,PX,Y,PY,T,PT,DELTAP,VOLT,LAG,HARMON,FREQ,E1,E2,APERTYPE,APER_1,APER_2,APER_3,APER_4,TILT,ANGLE;

SURVEY, file="survey_madx_z_b1.tfs";

TWISS, FILE = "twiss_z_b1_nottapered.tfs", TOLERANCE=1E-12; ! Twiss without radiation and tapering

!-------------------------------------------------------------------------------
! Get plain machine aperture
!-------------------------------------------------------------------------------
USE, SEQUENCE = L000017;
TWISS; ! Twiss without radiation and tapering
APERTURE, HALO={6,6,6,6}, COR=250e-6, DP=0, BBEAT=1.1, DPARX=0, DPARY=0, DQF=0.25, BETAQFX=93, FILE="aperture_z_b1_nottapered.tfs"; //on-momentum parameters
N1MIN_H = BEAM->N1MIN;

// Redefine beam with small horizontal emittance to find vertical bottleneck
BEAM, PARTICLE=POSITRON, NPART=Npar, KBUNCH=Nbun, ENERGY=Ebeam, RADIATE=FALSE, BV=+1, EX=EYbeam/1e5, EY=EYbeam;

USE, SEQUENCE = L000017;
TWISS; ! Twiss without radiation and tapering
APERTURE, HALO={6,6,6,6}, COR=250e-6, DP=0, BBEAT=1.1, DPARX=0, DPARY=0, DQF=0.25, BETAQFX=93, FILE="aperture_z_b1_nottapered_vertical.tfs"; //on-momentum parameters
N1MIN_V = BEAM->N1MIN;

BEAM, PARTICLE=POSITRON, NPART=Npar, KBUNCH=Nbun, ENERGY=Ebeam, RADIATE=FALSE, BV=+1, EX=EXbeam, EY=EYbeam;
USE, SEQUENCE = L000017;
TWISS; ! Twiss without radiation and tapering

!-------------------------------------------------------------------------------
! Add collimators
!-------------------------------------------------------------------------------
// SR collimators and masks
CALL, FILE="../sequences/MAD-X/aperture/install_synchrotron_rad_masks.madx";

// Halo collimators
SYSTEM, "python3 ../toolkit/place_collimators.py --operation_mode=z --tfs_file twiss_z_b1_nottapered.tfs --primary_position 10415 --plane H --collimator_opening 11 12 --madx_file ./install_collimators_H.madx --secondary_offsets -24 -39";
SYSTEM, "python3 ../toolkit/place_collimators.py --operation_mode=z --tfs_file twiss_z_b1_nottapered.tfs --primary_position 10316.5 --plane V --collimator_opening 50 65 --madx_file ./install_collimators_V.madx --secondary_offsets -40 35";
SYSTEM, "python3 ../toolkit/place_collimators.py --operation_mode=z --tfs_file twiss_z_b1_nottapered.tfs --primary_position 11642.5 --plane P --collimator_opening 18 28 --madx_file ./install_collimators_P.madx --secondary_offsets 0 0";
CALL, FILE='./install_collimators_H.madx';
CALL, FILE='./install_collimators_V.madx';
CALL, FILE='./install_collimators_P.madx';

USE, SEQUENCE = L000017;
TWISS, FILE = "twiss_z_b1_nottapered.tfs", TOLERANCE=1E-12; ! Twiss without radiation and tapering
APERTURE, HALO={6,6,6,6}, COR=250e-6, DP=0, BBEAT=1.1, DPARX=0, DPARY=0, DQF=0.25, BETAQFX=93, FILE="aperture_z_b1_nottapered_collimator.tfs"; //on-momentum parameters

THICK_QX = TABLE(SUMM, Q1);
THICK_QY = TABLE(SUMM, Q2);

!-------------------------------------------------------------------------------
! Install wigglers for vertical emittance generation
!-------------------------------------------------------------------------------
if (install_wigglers == 1) {
    CALL, FILE="install_wigglers.madx";
    EXEC, define_wigglers_as_kickers;
    EXEC, install_wigglers;

    CALL, FILE="../sequences/MAD-X/aperture/FCCee_aper_definitions.madx";

    USE, SEQUENCE = L000017;

    MATCH, SEQUENCE=L000017;
    VARY, NAME=K1QF4;
    VARY, NAME=K1QF2;
    VARY, NAME=K1QD3;
    VARY, NAME=K1QD1;

    GLOBAL, Q1=THICK_QX;
    GLOBAL, Q2=THICK_QY;

    LMDIF, CALLS=10000;
    JACOBIAN, CALLS=10000;
    ENDMATCH;

    TWISS, FILE="twiss_z_b1_nottapered_wiggler.tfs", TOLERANCE=1E-12;
}

!-------------------------------------------------------------------------------
! Save thick sequence
!-------------------------------------------------------------------------------
VOLTCA1 = VOLTCA1SAVE; 

SAVE, SEQUENCE=L000017, FILE="nottapered_z_b1.seq", BEAM=True;

VOLTCA1 = 0;

!-------------------------------------------------------------------------------
! Slice the lattice and save a thin sequence
!-------------------------------------------------------------------------------
// Note: if the tapering was enabled in the previous steps, MAKETHIN will inherit the
// tapered magnetic strenghts (KNTAP) calculated by TWISS and the element strengths (KN) accordingly to produce a tapered thin sequence.
// If any of: RF, beam radiation, tapering; were disabled before, the resulting thin sequence is not tapered.

// Slicing with special attention ot IR quads and sextupoles     
SELECT, FLAG=makethin, CLASS=RFCAVITY, SLICE = 1;
SELECT, FLAG=makethin, CLASS=rbend, SLICE = 10;
SELECT, FLAG=makethin, CLASS=quadrupole, SLICE = 10;
SELECT, FLAG=makethin, CLASS=sextupole, SLICE = 10;

SELECT, FLAG=makethin, PATTERN="^MB.*", SLICE=10;
SELECT, FLAG=makethin, PATTERN="^MQ.*", SLICE=10;
SELECT, FLAG=makethin, PATTERN="^QF.*", SLICE=5;
SELECT, FLAG=makethin, PATTERN="^QD.*", SLICE=5;
SELECT, FLAG=makethin, PATTERN="^QFG.*", SLICE=5;
SELECT, FLAG=makethin, PATTERN="^QDG.*", SLICE=5;
SELECT, FLAG=makethin, PATTERN="^QL.*", SLICE=5;
SELECT, FLAG=makethin, PATTERN="^QS.*", SLICE=5;
SELECT, FLAG=makethin, PATTERN="^QB.*", SLICE=10;
SELECT, FLAG=makethin, PATTERN="^QG.*", SLICE=10;
SELECT, FLAG=makethin, PATTERN="^QH.*", SLICE=10;
SELECT, FLAG=makethin, PATTERN="^QI.*", SLICE=10;
SELECT, FLAG=makethin, PATTERN="^QR.*", SLICE=10;
SELECT, FLAG=makethin, PATTERN="^QU.*", SLICE=10;
SELECT, FLAG=makethin, PATTERN="^QY.*", SLICE=10;
SELECT, FLAG=makethin, PATTERN="^QA.*", SLICE=50;
SELECT, FLAG=makethin, PATTERN="^QC.*", SLICE=50;
SELECT, FLAG=makethin, PATTERN="^SY.*", SLICE=20;

SELECT, FLAG=makethin, PATTERN="^TC.*", SLICE=1;

if (install_wigglers == 1) {
    SELECT, FLAG=makethin, PATTERN="^MWI.*", SLICE=25;
}
        
MAKETHIN, SEQUENCE=L000017, STYLE=TEAPOT, MAKEDIPEDGE=True;

USE, SEQUENCE = L000017;

MATCH, SEQUENCE=L000017;
VARY, NAME=K1QF4;
VARY, NAME=K1QF2;
VARY, NAME=K1QD3;
VARY, NAME=K1QD1;

GLOBAL, Q1=THICK_QX;
GLOBAL, Q2=THICK_QY;

LMDIF, CALLS=10000;
JACOBIAN, CALLS=10000;
ENDMATCH;
        
TWISS, FILE="twiss_z_b1_thin_nottapered.tfs", TOLERANCE=1E-12;

VOLTCA1 = VOLTCA1SAVE; 

SAVE, SEQUENCE=L000017, FILE="nottapered_z_b1_thin.seq", BEAM=True;
