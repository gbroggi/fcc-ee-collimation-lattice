# FCC-ee collimation lattice scripts

Collection of SAD and MAD-X scripts that help in common first analysis steps.
This scripts aim to provide newcomers simple examples which can then be adapted to the needs of the study.
Furthermore, the scripts are executed automatically every time a change in the gitlab repo occurs and their output is used to test that the lattices fulfill given requirements.

## Prerequisites

All scripts are reguarly checked that they work with the latest [MAD-X](https://madx.web.cern.ch/madx/) and [SAD](https://github.com/KatsOide/SAD) version.

## Scripts

- *SAD Scripts*
    - `check_co_*.sad` - returns two twiss files, one containing the optics functions and one containing the closed orbit.
    - `check_da_*.sad` - performs Dynamic aperture scans in the X-Y and X-Z plane for the given lattice. The returned files can be analysed using the `plot_da.py` script in the `toolkit` folder.
    - `check_optics_*.sad` - matches the tunes of the SAD sequence, returns a twiss file with the optics functions, as well as generating a MAD-X sequence from the matched SAD sequence
    - `check_offmom_optics_*.sad` - returns the optics for different levels of momentum offset in twiss files

- *MAD-X Scripts*
    - `fcc_ee_*.madx` - loads the MAD-X sequences and the aperture files from `sequences/MAD-X/aperture`, returns (offmomentum) optics, survey, and aperture for the case of no tapering and, optionally, when using tapering
