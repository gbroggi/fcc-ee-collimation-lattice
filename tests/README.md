# FCC-ee collimation lattice tests

This directory contains test-scripts using the `python-pytest` to check the output of SAD and MAD-X scripts from the `scripts` directory against known references.
These references are saved in the `reference_file.json`, and contain information such as the tunes, energy, minimum required DA for each working point.

## Prerequisites

The tests require the output from scripts in the `scripts` directory, which should be executed using the latest [MAD-X](https://madx.web.cern.ch/madx/) and [SAD](https://github.com/KatsOide/SAD) version.
Before running the tests, a `python3.7` installation is required, and packages should be installed using the following command.

```bash
pip install matplotlib numpy scipy tfs-pandas generic-parser pytest
```

The tests can then be run using

```bash
python -m pytest --operation_mode=OPMODE
```

where `OPMODE` refers to the FCC-ee operation mode and should be one of the following `z,w,h,t`.

## Scripts

- *test_madx.py*
    Tests that the emittance and beam energy defined in the MAD-X script correspond to the CDR values.
    Tests if tunes and beta_star are within a given tolerance to the reference values.
    Tests if the optics functions of the lattice are symmetric around the top-bottom axis.
    Tests if the minimum aperture of the ring is the primary collimator, and that all non-collimator elements have a beam-stay-clear above a defined level.
    Tests if the ring is closed using the MAD-X survey.

- *test_sad.py*
    Tests if tunes and beta_star are within a given tolerance to the reference values.
    Tests if the optics functions of the lattice are symmetric around the top-bottom axis.
    Tests if the DA both in X-Y and in X-Z is above the predefined level.
